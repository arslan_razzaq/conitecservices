<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagesController extends Controller
{
   public function home()
   {
   	return view('pages.home');;
   }

   public function price()
   {
   	return view('pages.price');
   }

   public function network()
   {
   	return view('pages.network-solution');
   }

   public function services()
   {
   	return view('pages.services-contract');
   }
   public function firewall()
   {
   	return view('pages.firewall');
   }

   public function contact()
   {
   	return view('pages.contact');
   }

    public function camera()
   {
   	return view('pages.camera-system');
   }

    public function business()
   {
   	return view('pages.business-support');
   }

}
