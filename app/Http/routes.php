<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('home','PagesController@home');
Route::get('price', 'PagesController@price');
Route::get('network-solution', 'PagesController@network');
Route::get('services-contract', 'PagesController@services');
Route::get('firewall', 'PagesController@firewall');
Route::get('contact', 'PagesController@contact');
Route::get('camera-system', 'PagesController@camera');
Route::get('business-support', 'PagesController@business');