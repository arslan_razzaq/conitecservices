<!-- Main Footer -->
    <footer class="main-footer">
		<div class="pattern-layer-one" style="background-image: url(images/background/pattern-7.png)"></div>
		<div class="pattern-layer-two" style="background-image: url(images/background/pattern-8.png)"></div>
		<!--Waves end-->
    	<div class="auto-container">
        	<!--Widgets Section-->
            <div class="widgets-section">
            	<div class="row clearfix">
                	
                    <!-- Column -->
                    <div class="big-column col-lg-6 col-md-12 col-sm-12">
						<div class="row clearfix">
						
                        	<!-- Footer Column -->
                            <div class="footer-column col-lg-7 col-md-6 col-sm-12">
                                <div class="footer-widget logo-widget">
									<div class="logo">
										<a href="index.html"><img src="images/logo-2.png" alt="" /></a>
									</div>
									<div class="text">We are the best  Information Technology Services Provider Company In Saudi Arabia . Providing the highest quality in hardware & Network solutions. About more than 15 years of experience and 1000 of innovative achievements.</div>
									<!-- Social Box -->
									<ul class="social-box">
										<li><a href="#" class="fa fa-facebook-f"></a></li>
										<li><a href="#" class="fa fa-linkedin"></a></li>
										<li><a href="https://api.whatsapp.com/send?phone=966540333650" target="_blank" class="fa fa-whatsapp"></a></li>

									</ul>
								</div>
							</div>
							
							<!-- Footer Column -->
                            <div class="footer-column col-lg-5 col-md-6 col-sm-12">
                                <div class="footer-widget links-widget">
									<h5>Quick Links</h5>
									<ul class="list-link">
										<li><a href="">Managed IT services</a></li>
										<li><a href="">Cloud Services</a></li>
										<li><a href="">IT support & helpdesk</a></li>
										<li><a href="">Cyber security</a></li>
										<li><a href="">Custom Software</a></li>
										<li><a href="">Free Consultation</a></li>
										<li><a href="">Our Business Growth</a></li>
									</ul>
								</div>
							</div>
							
						</div>
					</div>
					
					<!-- Column -->
                    <div class="big-column col-lg-6 col-md-12 col-sm-12">
						<div class="row clearfix">
							
							<!-- Footer Column -->
							<div class="footer-column col-lg-6 col-md-6 col-sm-12">
								<div class="footer-widget news-widget">
									<h5>Resent Post</h5>
									<!-- Footer Column -->
									<div class="widget-content">
										<div class="post">
											<div class="thumb"><a href="news-detail.html"><img src="images/resource/post-thumb-3.jpg" alt=""></a></div>
											<h6><a href="news-detail.html">Define World Best IT Solution Technology</a></h6>
											<span class="date">May 01, 2020</span>
										</div>

										<div class="post">
											<div class="thumb"><a href="news-detail.html"><img src="images/resource/post-thumb-4.jpg" alt=""></a></div>
											<h6><a href="news-detail.html">PHP Frameworks You Need To Use Anywhere</a></h6>
											<span class="date">May 01, 2020</span>
										</div>
									</div>
								</div>
							</div>
							
							<!-- Footer Column -->
							<div class="footer-column col-lg-6 col-md-6 col-sm-12">
								<div class="footer-widget contact-widget">
									<h5>Contact Us</h5>
									<ul>
										<li>
											<span class="icon flaticon-placeholder-2"></span>
											<strong>Address</strong>
											Dabab Street, Riyadh Saudi Arabia

										</li>
										<li>
											<span class="icon flaticon-phone-call"></span>
											<strong>Phone</strong>
											<a href="tel:+966-11-293 0449">+966-11-293 0449 </a>
										</li>
										<li>
											<span class="icon flaticon-phone-call"></span>
											<strong>Mobile</strong>
											<a href="tel:+966-540333650">+966-540333650 </a>
										</li>
										<li>
											<span class="icon flaticon-email-1"></span>
											<strong>E-Mail</strong>
											<a href="mailto:support@conitecservices.com">support@conitecservices.com</a>
										</li>
									</ul>
								</div>
							</div>
							
						</div>
					</div>
					
				</div>
			</div>
			
			<!-- Footer Bottom -->
			<div class="footer-bottom">
				<div class="auto-container">
					<div class="row clearfix">
						<!-- Column -->
						<div class="column col-lg-6 col-md-12 col-sm-12">
							<div class="copyright">Copyright &copy; 2020 Conitec Services  All Rights Reserved.</div>
						</div>
						<!-- Column -->
						<div class="column col-lg-6 col-md-12 col-sm-12">
							<ul class="footer-nav">
								<li><a href="#">About Us</a></li>
								<li><a href="#">Services</a></li>
								<li><a href="#">Privacy</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</footer>
	
	
</div>
<!--End pagewrapper-->



<!-- Search Popup -->
<div class="search-popup">
	<button class="close-search style-two"><span class="flaticon-multiply"></span></button>
	<button class="close-search"><span class="flaticon-up-arrow-1"></span></button>
	<form method="post" action="blog.html">
		<div class="form-group">
			<input type="search" name="search-field" value="" placeholder="Search Here" required="">
			<button type="submit"><i class="fa fa-search"></i></button>
		</div>
	</form>
</div>
<!-- End Header Search -->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-arrow-up"></span></div>
<script src="js/jquery.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/appear.js"></script>
<script src="js/parallax.min.js"></script>
<script src="js/tilt.jquery.min.js"></script>
<script src="js/jquery.paroller.min.js"></script>
<script src="js/owl.js"></script>
<script src="js/wow.js"></script>
<script src="js/nav-tool.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/script.js"></script>
<script src="js/color-settings.js"></script>

