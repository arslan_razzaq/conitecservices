@extends('layout.app')

@section('title', 'Business Support')



@section('content')

<!--Page Title-->
    <section class="page-title">
		<div class="pattern-layer-one" style="background-image: url(images/background/pattern-16.png)"></div>
    	<div class="auto-container">
			<h2>Business Support </h2>
			<h4> We will manage, monitor and support all your business technology. One Simple Price. </h4>
			<ul class="page-breadcrumb">
				<li><a href="index.html">home</a></li>
				<li>Business Support </li>
			</ul>
        </div>
    </section>
    <!--End Page Title-->
	
	<!-- About Section -->
	<section class="about-section">
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="sec-title">
				
				<h2 class="text-center">WHY HIRE A SINGLE IT PERSON WHEN YOU CAN HAVE A STAFF OF EXPERTS?</h2>
			</div>
			<div class="row clearfix">
				<div class="col-md-4 card-container">
			      <div class="card-flip">
			        <!-- Card 1 Front -->
			        <div class="card front">
			          
			          <div class="card-block">
			            <h4 class="card-title text-center"> Unlimited Support Contract.</h4>
			            <p class="card-text"> > </p>
			          </div> 
			        </div>
			        <!-- End Card 1 Front -->
			        <!-- Card 1 Back -->
			        <div class="card back">
			            <p class="card-text">Our support contracts can be tailored to meet your general business or specific project’s needs. We offer remote and on-site services with guaranteed response times. Whether it is during normal business hours, after hours or on the weekends, our friendly skilled technicians are here to get the job done.</p>
			        </div>
			        <!-- End Card 1 Back -->
			      </div>
    			</div>
    		<!-- End Card 1 -->

    			<div class="col-md-4 card-container">
			      <div class="card-flip">
			        <!-- Card 2 Front -->
			        <div class="card front">
			          
			          <div class="card-block">
			            <h4 class="card-title text-center">  24 x 7 Network Management</h4>
			            <p class="card-text"> > </p>
			          </div> 
			        </div>
			        <!-- End Card 2 Front -->
			        <!-- Card 2 Back -->
			        <div class="card back">
			            <p class="card-text">We offer complete support and management of your entire network infrastructure including servers, desktop computers, laptops, smartphones and tablets, internet services, printers and more. If there are any serious disruptions to your network or internet, we address it immediately. We believe in addressing small problems before they create bigger issues.</p>
			        </div>
			        <!-- End Card 2 Back -->
			      </div>
    			</div>
    		<!-- End Card 2 -->

    		<div class="col-md-4 card-container">
			      <div class="card-flip">
			        <!-- Card 3 Front -->
			        <div class="card front">
			          
			          <div class="card-block">
			            <h4 class="card-title text-center"> Network Cabling Installation </h4>
			            <p class="card-text"> > </p>
			          </div> 
			        </div>
			        <!-- End Card 3 Front -->
			        <!-- Card 3 Back -->
			        <div class="card back">
			            <p class="card-text">Whether you have a brand new office that needs to be wired, or you are just adding an additional network jack; we can get the job done. No need to search out and hire a vendor who does not understand your current network infrastructure or business needs. Our licensed installers are experienced in installing all types of low voltage CAT5/6, Fiber Optic, COAX and speaker wire.</p>
			        </div>
			        <!-- End Card 3 Back -->
			      </div>
    			</div>
    		<!-- End Card 3 -->
			</div>
			<div class="row clearfix">
				<div class="col-md-4 card-container">
			      <div class="card-flip">
			        <!-- Card 4 Front -->
			        <div class="card front">
			          
			          <div class="card-block">
			            <h4 class="card-title text-center"> Network & Endpoint Security </h4>
			            <p class="card-text"> > </p>
			          </div> 
			        </div>
			        <!-- End Card 4 Front -->
			        <!-- Card 4 Back -->
			        <div class="card back">
			            <p class="card-text">By implementing the latest solutions for email and web-based threats, we will make sure that your network is secure from hackers and breaches. This protects your devices and ensures users are not susceptible to viruses, malware, and other cyber threats.</p>
			        </div>
			        <!-- End Card 4 Back -->
			      </div>
    			</div>
    		<!-- End Card 4 -->
			

			<div class="col-md-4 card-container">
			      <div class="card-flip">
			        <!-- Card 5 Front -->
			        <div class="card front">
			          
			          <div class="card-block">
			            <h4 class="card-title text-center"> Remote Access Solutions </h4>
			            <p class="card-text"> > </p>
			          </div> 
			        </div>
			        <!-- End Card 5 Front -->
			        <!-- Card 5 Back -->
			        <div class="card back">
			            <p class="card-text">Working from home has become a requirement in today’s workforce, especially now more than ever. We have the experience and expertise to quickly implement a solution that works best for your business. Whether that solution is a traditional, secure VPN or a simpler screen sharing alternative, we can help recommend the right solution for your business.</p>
			        </div>
			        <!-- End Card 5 Back -->
			      </div>
    			</div>
    		<!-- End Card 5 -->
			

			<div class="col-md-4 card-container">
			      <div class="card-flip">
			        <!-- Card 6 Front -->
			        <div class="card front">
			          
			          <div class="card-block">
			            <h4 class="card-title text-center"> Vendor Liaison Assistance </h4>
			            <p class="card-text"> > </p>
			          </div> 
			        </div>
			        <!-- End Card 6 Front -->
			        <!-- Card 6 Back -->
			        <div class="card back">
			            <p class="card-text">We’ll take vendor management off your hands, working directly with your software and technology providers to handle issues with their services. This includes business specific applications, internet service providers, telecom service providers, and even your printer/copier leasing company.</p>
			        </div>
			        <!-- End Card 6 Back -->
			      </div>
    			</div>
    		<!-- End Card 6 -->

			</div>
			<br>
			<div class="sec-title">
				
				<h2 class="text-center">BOOST PRODUCTIVITY AND PROFITABILITY WITH IT THAT SUPPORTS YOUR BUSINESS GOALS</h2>
			</div>
				<div class="btn-box text-center">
				<a href="contact.html" class="theme-btn btn-style-three"><span class="txt">Contact Us </span></a>
				</div>
		</div>
	</section>
	<!-- End About Section -->
	
	
	
	
	
	
	
	<!-- Technology Section -->
	<section class="technology-section style-two" style="background-image: url(images/background/1.jpg)">
		<div class="pattern-layer-one" style="background-image: url(images/background/pattern-5.png)"></div>
		<div class="pattern-layer-two" style="background-image: url(images/background/pattern-6.png)"></div>
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="sec-title light centered">
				<div class="title">TECHNOLOGY INDEX</div>
				<h2>Real Time Monitoring Your Infrstracture <br> Branded Degital Solutions</h2>
			</div>
			<div class="row clearfix">
				
				<!-- Technology Block -->
				<div class="technology-block">
					<div class="inner-box">
						<a href="services-detail.html" class="overlay-link"></a>
						<div class="icon-box">
							<span class="flaticon-dashboard"></span>
						</div>
						<h6>Data Analytics</h6>
					</div>
				</div>
				
				<!-- Technology Block -->
				<div class="technology-block">
					<div class="inner-box">
						<a href="services-detail.html" class="overlay-link"></a>
						<div class="icon-box">
							<span class="flaticon-coding-3"></span>
						</div>
						<h6>Web Develpment</h6>
					</div>
				</div>
				
				<!-- Technology Block -->
				<div class="technology-block">
					<div class="inner-box">
						<a href="services-detail.html" class="overlay-link"></a>
						<div class="icon-box">
							<span class="flaticon-design"></span>
						</div>
						<h6>Ul/UX Design</h6>
					</div>
				</div>
				
				<!-- Technology Block -->
				<div class="technology-block">
					<div class="inner-box">
						<a href="services-detail.html" class="overlay-link"></a>
						<div class="icon-box">
							<span class="flaticon-web-browser"></span>
						</div>
						<h6>QA & Testing</h6>
					</div>
				</div>
				
				<!-- Technology Block -->
				<div class="technology-block">
					<div class="inner-box">
						<a href="services-detail.html" class="overlay-link"></a>
						<div class="icon-box">
							<span class="flaticon-network-1"></span>
						</div>
						<h6>Dedicated Team</h6>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!-- End Technology Section -->
	
	<!-- Experiance Section -->
	<section class="experiance-section" style="background-image: url(images/background/pattern-9.png)">
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="sec-title centered">
				<div class="title">EXPERIENCE. EXECUTION. EXCELLENCE.</div>
				<h2>What We Actually Do</h2>
			</div>
			
			<!-- Experiance Info Tabs -->
			<div class="experiance-info-tabs">
				<!-- Experiance Tabs -->
				<div class="experiance-tabs tabs-box">
				
					<!-- Tab Btns-->
					<ul class="tab-btns tab-buttons clearfix">
						<li data-tab="#prod-html" class="tab-btn"><span class="icon flaticon-html"></span>HTML</li>
						<li data-tab="#prod-bootstrap" class="tab-btn"><span class="icon flaticon-bootstrap"></span>Bootstrap</li>
						<li data-tab="#prod-css" class="tab-btn active-btn"><span class="icon flaticon-css"></span>CSS</li>
						<li data-tab="#prod-php" class="tab-btn"><span class="icon flaticon-php"></span>Php</li>
						<li data-tab="#prod-java" class="tab-btn"><span class="icon flaticon-java"></span>JavaScript</li>
					</ul>
					
					<!--Tabs Container-->
					<div class="tabs-content">
						
						<!--Tab / Active Tab-->
						<div class="tab" id="prod-html">
							<div class="content">
								<h4>HTML Website Development Services</h4>
								<div class="text">Conitec is the partner of choice for many of the world’s leading enterprises, SMEs and technology challengers. We help businesses <br> elevate their value through custom software development, product design, QA and consultancy services.</div>
								<div class="btn-box text-center">
									<a href="services-detail.html" class="theme-btn btn-style-three"><span class="txt">Learn More</span></a>
								</div>
							</div>
						</div>
						
						<!-- Tab -->
						<div class="tab" id="prod-bootstrap">
							<div class="content">
								<h4>Bootstrap Website Development Services</h4>
								<div class="text">Conitec is the partner of choice for many of the world’s leading enterprises, SMEs and technology challengers. We help businesses <br> elevate their value through custom software development, product design, QA and consultancy services.</div>
								<div class="btn-box text-center">
									<a href="services-detail.html" class="theme-btn btn-style-three"><span class="txt">Learn More</span></a>
								</div>
							</div>
						</div>
						
						<!-- Tab -->
						<div class="tab active-tab" id="prod-css">
							<div class="content">
								<h4>CSS Website Development Services</h4>
								<div class="text">Conitec is the partner of choice for many of the world’s leading enterprises, SMEs and technology challengers. We help businesses <br> elevate their value through custom software development, product design, QA and consultancy services.</div>
								<div class="btn-box text-center">
									<a href="services-detail.html" class="theme-btn btn-style-three"><span class="txt">Learn More</span></a>
								</div>
							</div>
						</div>
						
						<!-- Tab -->
						<div class="tab" id="prod-php">
							<div class="content">
								<h4>Php Website Development Services</h4>
								<div class="text">Conitec is the partner of choice for many of the world’s leading enterprises, SMEs and technology challengers. We help businesses <br> elevate their value through custom software development, product design, QA and consultancy services.</div>
								<div class="btn-box text-center">
									<a href="services-detail.html" class="theme-btn btn-style-three"><span class="txt">Learn More</span></a>
								</div>
							</div>
						</div>
						
						<!-- Tab -->
						<div class="tab" id="prod-java">
							<div class="content">
								<h4>JavaScript Website Development Services</h4>
								<div class="text">Engitech is the partner of choice for many of the world’s leading enterprises, SMEs and technology challengers. We help businesses <br> elevate their value through custom software development, product design, QA and consultancy services.</div>
								<div class="btn-box text-center">
									<a href="services-detail.html" class="theme-btn btn-style-three"><span class="txt">Learn More</span></a>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!-- End Experiance Section -->

@endsection