@extends('layout.app')

@section('title', 'Firewall Installation')



@section('content')

<!-- About Section -->
	<section class="about-section">
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="sec-title">
				
				<h2>What is a Next-Generation Firewall?  </h2>
				<div class="text">
				Next-generation firewalls filter network traffic to protect an organization from external threats. Maintaining features of stateful firewalls such as packet filtering, VPN support, network monitoring, and IP mapping features, NGFWs also possess deeper inspection capabilities that give them a superior ability to identify attacks, malware, and other threats. Next-generation firewalls provide organizations with application control, intrusion prevention, and advanced visibility across the network. As the threat landscape continues to develop rapidly, traditional firewalls fall further behind and put your organization at risk. NGFWs not only block malware, but also include paths for future updates, giving them the flexibility to evolve with the landscape and keep the network secure as new threats arise.
			</div>

				
			 	
			 
				
				<br>

				<div class="btn-box text-center">
				<a href="contact.html" class="theme-btn btn-style-three"><span class="txt">Contact Us </span></a>
				</div>
		</div>
	</section>
	<!-- End About Section -->
	
	
	
	
	
	
	
	<!-- Technology Section -->
	<section class="technology-section style-two" style="background-image: url(images/background/1.jpg)">
		<div class="pattern-layer-one" style="background-image: url(images/background/pattern-5.png)"></div>
		<div class="pattern-layer-two" style="background-image: url(images/background/pattern-6.png)"></div>
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="sec-title light centered">
				<div class="title">TECHNOLOGY INDEX</div>
				<h2>Real Time Monitoring Your Infrstracture <br> Branded Degital Solutions</h2>
			</div>
			<div class="row clearfix">
				
				<!-- Technology Block -->
				<div class="technology-block">
					<div class="inner-box">
						<a href="services-detail.html" class="overlay-link"></a>
						<div class="icon-box">
							<span class="flaticon-dashboard"></span>
						</div>
						<h6>Data Analytics</h6>
					</div>
				</div>
				
				<!-- Technology Block -->
				<div class="technology-block">
					<div class="inner-box">
						<a href="services-detail.html" class="overlay-link"></a>
						<div class="icon-box">
							<span class="flaticon-coding-3"></span>
						</div>
						<h6>Web Develpment</h6>
					</div>
				</div>
				
				<!-- Technology Block -->
				<div class="technology-block">
					<div class="inner-box">
						<a href="services-detail.html" class="overlay-link"></a>
						<div class="icon-box">
							<span class="flaticon-design"></span>
						</div>
						<h6>Ul/UX Design</h6>
					</div>
				</div>
				
				<!-- Technology Block -->
				<div class="technology-block">
					<div class="inner-box">
						<a href="services-detail.html" class="overlay-link"></a>
						<div class="icon-box">
							<span class="flaticon-web-browser"></span>
						</div>
						<h6>QA & Testing</h6>
					</div>
				</div>
				
				<!-- Technology Block -->
				<div class="technology-block">
					<div class="inner-box">
						<a href="services-detail.html" class="overlay-link"></a>
						<div class="icon-box">
							<span class="flaticon-network-1"></span>
						</div>
						<h6>Dedicated Team</h6>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!-- End Technology Section -->
	
	<!-- Experiance Section -->
	<section class="experiance-section" style="background-image: url(images/background/pattern-9.png)">
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="sec-title centered">
				<div class="title">EXPERIENCE. EXECUTION. EXCELLENCE.</div>
				<h2>What We Actually Do</h2>
			</div>
			
			<!-- Experiance Info Tabs -->
			<div class="experiance-info-tabs">
				<!-- Experiance Tabs -->
				<div class="experiance-tabs tabs-box">
				
					<!-- Tab Btns-->
					<ul class="tab-btns tab-buttons clearfix">
						<li data-tab="#prod-html" class="tab-btn"><span class="icon flaticon-html"></span>HTML</li>
						<li data-tab="#prod-bootstrap" class="tab-btn"><span class="icon flaticon-bootstrap"></span>Bootstrap</li>
						<li data-tab="#prod-css" class="tab-btn active-btn"><span class="icon flaticon-css"></span>CSS</li>
						<li data-tab="#prod-php" class="tab-btn"><span class="icon flaticon-php"></span>Php</li>
						<li data-tab="#prod-java" class="tab-btn"><span class="icon flaticon-java"></span>JavaScript</li>
					</ul>
					
					<!--Tabs Container-->
					<div class="tabs-content">
						
						<!--Tab / Active Tab-->
						<div class="tab" id="prod-html">
							<div class="content">
								<h4>HTML Website Development Services</h4>
								<div class="text">Conitec is the partner of choice for many of the world’s leading enterprises, SMEs and technology challengers. We help businesses <br> elevate their value through custom software development, product design, QA and consultancy services.</div>
								<div class="btn-box text-center">
									<a href="services-detail.html" class="theme-btn btn-style-three"><span class="txt">Learn More</span></a>
								</div>
							</div>
						</div>
						
						<!-- Tab -->
						<div class="tab" id="prod-bootstrap">
							<div class="content">
								<h4>Bootstrap Website Development Services</h4>
								<div class="text">Conitec is the partner of choice for many of the world’s leading enterprises, SMEs and technology challengers. We help businesses <br> elevate their value through custom software development, product design, QA and consultancy services.</div>
								<div class="btn-box text-center">
									<a href="services-detail.html" class="theme-btn btn-style-three"><span class="txt">Learn More</span></a>
								</div>
							</div>
						</div>
						
						<!-- Tab -->
						<div class="tab active-tab" id="prod-css">
							<div class="content">
								<h4>CSS Website Development Services</h4>
								<div class="text">Conitec is the partner of choice for many of the world’s leading enterprises, SMEs and technology challengers. We help businesses <br> elevate their value through custom software development, product design, QA and consultancy services.</div>
								<div class="btn-box text-center">
									<a href="services-detail.html" class="theme-btn btn-style-three"><span class="txt">Learn More</span></a>
								</div>
							</div>
						</div>
						
						<!-- Tab -->
						<div class="tab" id="prod-php">
							<div class="content">
								<h4>Php Website Development Services</h4>
								<div class="text">Conitec is the partner of choice for many of the world’s leading enterprises, SMEs and technology challengers. We help businesses <br> elevate their value through custom software development, product design, QA and consultancy services.</div>
								<div class="btn-box text-center">
									<a href="services-detail.html" class="theme-btn btn-style-three"><span class="txt">Learn More</span></a>
								</div>
							</div>
						</div>
						
						<!-- Tab -->
						<div class="tab" id="prod-java">
							<div class="content">
								<h4>JavaScript Website Development Services</h4>
								<div class="text">Engitech is the partner of choice for many of the world’s leading enterprises, SMEs and technology challengers. We help businesses <br> elevate their value through custom software development, product design, QA and consultancy services.</div>
								<div class="btn-box text-center">
									<a href="services-detail.html" class="theme-btn btn-style-three"><span class="txt">Learn More</span></a>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!-- End Experiance Section -->
@endsection