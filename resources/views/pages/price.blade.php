@extends('layout.app')

@section('title', 'Leading IT Services Provider In Saudi Arabia')



@section('content')
<!--Page Title-->
    <section class="page-title">
		<div class="pattern-layer-one" style="background-image: url(images/background/pattern-16.png)"></div>
    	<div class="auto-container">
			<h2>Price plan</h2>
			<ul class="page-breadcrumb">
				<li><a href="index.html">home</a></li>
				<li>Pricing</li>
			</ul>
        </div>
    </section>
    <!--End Page Title-->
	
	<!-- Pricing Section -->
	<section class="pricing-section">
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="sec-title centered">
				<div class="title">Our Pricing Plans</div>
				<h2>A Monthly Project Fee <br> Price Plans</h2>
			</div>
			
			<div class="row clearfix">
			
				<!-- Price Block -->
				<div class="price-block col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box">
						<h3>Basic</h3>
						<div class="text">Designed for businesses with standard health requirements</div>
						<div class="price">$ 70.00 <span>/ Per Month</span></div>
						<ul class="price-list">
							<li>24/7 Support</li>
							<li>Advanced Options</li>
							<li>16 GB Storage</li>
							<li>Unlimited Support</li>
						</ul>
						<div class="btn-box">
							<a href="contact.html" class="theme-btn btn-style-one"><span class="txt">Get Started</span></a>
						</div>
					</div>
				</div>
				
				<!-- Price Block -->
				<div class="price-block col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box">
						<h3>Standard</h3>
						<div class="text">Designed for businesses with standard health requirements</div>
						<div class="price">$ 90.00 <span>/ Per Month</span></div>
						<ul class="price-list">
							<li>24/7 Support</li>
							<li>Advanced Options</li>
							<li>16 GB Storage</li>
							<li>Unlimited Support</li>
						</ul>
						<div class="btn-box">
							<a href="contact.html" class="theme-btn btn-style-one"><span class="txt">Get Started</span></a>
						</div>
					</div>
				</div>
				
				<!-- Price Block -->
				<div class="price-block col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box">
						<h3>Professional</h3>
						<div class="text">Designed for businesses with standard health requirements</div>
						<div class="price">$ 100.00 <span>/ Per Month</span></div>
						<ul class="price-list">
							<li>24/7 Support</li>
							<li>Advanced Options</li>
							<li>16 GB Storage</li>
							<li>Unlimited Support</li>
						</ul>
						<div class="btn-box">
							<a href="contact.html" class="theme-btn btn-style-one"><span class="txt">Get Started</span></a>
						</div>
					</div>
				</div>
				
			</div>
			
		</div>
	</section>
	<!-- End Pricing Section -->
	
	<!-- Appointment Section -->
	<section class="appointment-section style-two">
		<div class="image-layer" style="background-image: url(images/background/4.jpg)"></div>
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="sec-title light centered">
				<div class="title">CONTACT US</div>
				<h2>Join Us To Get IT Free <br> Consultations</h2>
			</div>
			<div class="inner-container">
				<div class="row clearfix">
					
					<!-- Image Column -->
					<div class="image-column col-lg-6 col-md-12 col-sm-12">
						<div class="inner-column wow slideInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="image">
								<img src="images/resource/appointment.jpg" alt="" />
							</div>
						</div>
					</div>
					
					<!-- Form Column -->
					<div class="form-column col-lg-6 col-md-12 col-sm-12">
						<div class="inner-column">
							<h4>You Don't Hesitate To Contact <br> With Us, Now Say Hello......</h4>
							<!-- Appointment Form -->
							<div class="appointment-form">
								<form method="post" action="appointment.html">
									<div class="row clearfix">
										<div class="col-lg-6 col-md-6 col-sm-12 form-group">
											<input type="text" name="username" placeholder="Name" required="">
											<span class="icon fa fa-user"></span>
										</div>
										
										<div class="col-lg-6 col-md-6 col-sm-12 form-group">
											<input type="email" name="email" placeholder="Email" required="">
											<span class="icon fa fa-envelope"></span>
										</div>

										<div class="col-lg-6 col-md-6 col-sm-12 form-group">
											<input type="tel" name="phone" placeholder="Phone No" required="">
											<span class="icon fa fa-phone"></span>
										</div>

										<div class="col-lg-6 col-md-6 col-sm-12 form-group">
											<input type="text" name="department" placeholder="Department" required="">
											<span class="icon fa fa-home"></span>
										</div>

										<div class="col-lg-12 col-md-12 col-sm-12 form-group">
											<textarea name="message" placeholder="Message"></textarea>
										</div>

										<div class="col-lg-12 col-md-12 col-sm-12 form-group">
											<button class="theme-btn btn-style-three" type="submit" name="submit-form"><span class="txt">Send Massage</span></button>
										</div>
									</div>
								</form>
							</div>
							
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</section>
	
	<!-- Info Section -->
	<section class="info-section" style="background-image: url(images/background/6.jpg)">
		<div class="auto-container">
			<div class="row clearfix">
				
				<!-- Logo Column -->
				<div class="logo-column col-lg-3 col-md-6 col-sm-12">
					<div class="inner-column">
						<div class="logo">
							<a href="index.html"><img src="images/logo-2.png" alt="" /></a>
						</div>
					</div>
				</div>
				
				<!-- Info Column -->
				<div class="info-column col-lg-3 col-md-6 col-sm-12">
					<div class="inner-column">
						<div class="icon-box"><span class="flaticon-pin"></span></div>
						<ul>
							<li><strong>Address</strong></li>
							<li>125, Suitland Street, USA</li>
						</ul>
					</div>
				</div>
				
				<!-- Info Column -->
				<div class="info-column col-lg-3 col-md-6 col-sm-12">
					<div class="inner-column">
						<div class="icon-box"><span class="flaticon-phone-call"></span></div>
						<ul>
							<li><strong>Phone</strong></li>
							<li>+ 786 875 864 75</li>
						</ul>
					</div>
				</div>
				
				<!-- Info Column -->
				<div class="info-column col-lg-3 col-md-6 col-sm-12">
					<div class="inner-column">
						<div class="icon-box"><span class="flaticon-email-1"></span></div>
						<ul>
							<li><strong>E-Mail</strong></li>
							<li>support@globex.com</li>
						</ul>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!-- End Info Section -->

	@endsection