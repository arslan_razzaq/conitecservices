@extends('layout.app')

@section('title', 'Camere Surveillance')



@section('content')

<!--Page Title-->
    <section class="page-title">
		<div class="pattern-layer-one" style="background-image: url(images/background/pattern-16.png)"></div>
    	<div class="auto-container">
			<h2>CAMERA SURVEILLANCE </h2>
			<h4>Professional Camera Surveillance Installation for Business and Home </h4>
			<ul class="page-breadcrumb">
				<li><a href="index.html">home</a></li>
				<li>Camere Surveillance </li>
			</ul>
        </div>
    </section>
    <!--End Page Title-->
	
	<!-- About Section -->
	<section class="about-section">
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="row clearfix">
				<div class="content-column col-lg-6 col-md-12 col-sm-12">
					<div class="sec-title">
					<h2>IP CAMERA VIDEO SYSTEMS </h2>
					</div>
						<div class="sec-title">
						<h5> When your goal is to keep your customers, employees, and company assets safe, it pays to have a reliable video camera company at your fingertips.</h5>
						<br> 
						<h5> Let us design an affordable IP Camera Video Systems for your home or office. We are a NJ State licensed camera installation company who can design, install, and maintain the right camera system for your home or office.</h5>
						</div>
				</div>
				<div class="content-column col-lg-6 col-md-12 col-sm-12">
					<img src="images/resource/video.jpg">
				</div>
			</div>
			<br> <br>  <br>

			<div class="row clearfix">
				<div class="content-column col-lg-6 col-md-12 col-sm-12">
					<div class="sec-title">
					<h4>WHETHER YOU NEED A CAMERA SYSTEM FOR YOUR HOME OR YOUR OFFICE, WE CAN HELP YOU DESIGN THE RIGHT SYSTEM FOR YOUR BUDGET. </h4>
					</div>
						<div class="sec-title">
						<h5> Local businesses and homeowners have turned to us for all their state-of-the-art IP camera needs, and so should you. Since 2005, we have been designing, installing and maintaining advanced IP camera technologies. </h5>
						</div>
				</div>
				<div class="content-column col-lg-6 col-md-12 col-sm-12">
					<br> <br> <br> <br>
					<div class="text">
						<h5>  <p> <i class="fa fa-check-circle" aria-hidden="true"></i> Free smartphone access of all your cameras </p> </h5>
						<h5> <p> <i class="fa fa-check-circle" aria-hidden="true"></i> Night vision is standard on all cameras</p> </h5>
						<h5> <p> <i class="fa fa-check-circle" aria-hidden="true"></i> Be notified if motion is detected on a camera </p> </h5>
						<h5> <p> <i class="fa fa-check-circle" aria-hidden="true"></i> Record audio and video </p> </h5>
					</div>
					
				</div>
			</div>
			<br> <br>
			<div class="row clearfix">
				<div class="content-column col-lg-6 col-md-12 col-sm-12">
					<div class="sec-title">
					<h2>GET A  QUOTE NOW ! </h2>
					</div>
					<div class="text">
						<h5>  <p> <i class="fa fa-check-circle" aria-hidden="true"></i> Close to 15 years of experience (since 2005) </p> </h5>
						<h5> <p> <i class="fa fa-check-circle" aria-hidden="true"></i> Licensed and insured </p> </h5>
						<h5> <p> <i class="fa fa-check-circle" aria-hidden="true"></i> Service contracts available</p> </h5>
						<h5> <p> <i class="fa fa-check-circle" aria-hidden="true"></i> Record audio and video </p> </h5>
						<h5> <p> <i class="fa fa-check-circle" aria-hidden="true"></i> Home Automation Specialists</p> </h5>
						<h5> <p> <i class="fa fa-check-circle" aria-hidden="true"></i> We provide professional, custom and clean installs </p> </h5>
						
					</div>
				</div>
				<div class="content-column col-lg-6 col-md-12 col-sm-12">
					<br> <br> 
					<img src="images/resource/camere.jpg">
					
				</div>
			</div>
			
				<br>
				<div class="btn-box text-center">
				<a href="contact.html" class="theme-btn btn-style-three"><span class="txt">Contact Us </span></a>
				</div>
		</div>
	</section>
	<!-- End About Section -->
	
	
	
	
	
	
	
	<!-- Technology Section -->
	<section class="technology-section style-two" style="background-image: url(images/background/1.jpg)">
		<div class="pattern-layer-one" style="background-image: url(images/background/pattern-5.png)"></div>
		<div class="pattern-layer-two" style="background-image: url(images/background/pattern-6.png)"></div>
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="sec-title light centered">
				<div class="title">TECHNOLOGY INDEX</div>
				<h2>Real Time Monitoring Your Infrstracture <br> Branded Degital Solutions</h2>
			</div>
			<div class="row clearfix">
				
				<!-- Technology Block -->
				<div class="technology-block">
					<div class="inner-box">
						<a href="services-detail.html" class="overlay-link"></a>
						<div class="icon-box">
							<span class="flaticon-dashboard"></span>
						</div>
						<h6>Data Analytics</h6>
					</div>
				</div>
				
				<!-- Technology Block -->
				<div class="technology-block">
					<div class="inner-box">
						<a href="services-detail.html" class="overlay-link"></a>
						<div class="icon-box">
							<span class="flaticon-coding-3"></span>
						</div>
						<h6>Web Develpment</h6>
					</div>
				</div>
				
				<!-- Technology Block -->
				<div class="technology-block">
					<div class="inner-box">
						<a href="services-detail.html" class="overlay-link"></a>
						<div class="icon-box">
							<span class="flaticon-design"></span>
						</div>
						<h6>Ul/UX Design</h6>
					</div>
				</div>
				
				<!-- Technology Block -->
				<div class="technology-block">
					<div class="inner-box">
						<a href="services-detail.html" class="overlay-link"></a>
						<div class="icon-box">
							<span class="flaticon-web-browser"></span>
						</div>
						<h6>QA & Testing</h6>
					</div>
				</div>
				
				<!-- Technology Block -->
				<div class="technology-block">
					<div class="inner-box">
						<a href="services-detail.html" class="overlay-link"></a>
						<div class="icon-box">
							<span class="flaticon-network-1"></span>
						</div>
						<h6>Dedicated Team</h6>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!-- End Technology Section -->
	
	<!-- Experiance Section -->
	<section class="experiance-section" style="background-image: url(images/background/pattern-9.png)">
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="sec-title centered">
				<div class="title">EXPERIENCE. EXECUTION. EXCELLENCE.</div>
				<h2>What We Actually Do</h2>
			</div>
			
			<!-- Experiance Info Tabs -->
			<div class="experiance-info-tabs">
				<!-- Experiance Tabs -->
				<div class="experiance-tabs tabs-box">
				
					<!-- Tab Btns-->
					<ul class="tab-btns tab-buttons clearfix">
						<li data-tab="#prod-html" class="tab-btn"><span class="icon flaticon-html"></span>HTML</li>
						<li data-tab="#prod-bootstrap" class="tab-btn"><span class="icon flaticon-bootstrap"></span>Bootstrap</li>
						<li data-tab="#prod-css" class="tab-btn active-btn"><span class="icon flaticon-css"></span>CSS</li>
						<li data-tab="#prod-php" class="tab-btn"><span class="icon flaticon-php"></span>Php</li>
						<li data-tab="#prod-java" class="tab-btn"><span class="icon flaticon-java"></span>JavaScript</li>
					</ul>
					
					<!--Tabs Container-->
					<div class="tabs-content">
						
						<!--Tab / Active Tab-->
						<div class="tab" id="prod-html">
							<div class="content">
								<h4>HTML Website Development Services</h4>
								<div class="text">Conitec is the partner of choice for many of the world’s leading enterprises, SMEs and technology challengers. We help businesses <br> elevate their value through custom software development, product design, QA and consultancy services.</div>
								<div class="btn-box text-center">
									<a href="services-detail.html" class="theme-btn btn-style-three"><span class="txt">Learn More</span></a>
								</div>
							</div>
						</div>
						
						<!-- Tab -->
						<div class="tab" id="prod-bootstrap">
							<div class="content">
								<h4>Bootstrap Website Development Services</h4>
								<div class="text">Conitec is the partner of choice for many of the world’s leading enterprises, SMEs and technology challengers. We help businesses <br> elevate their value through custom software development, product design, QA and consultancy services.</div>
								<div class="btn-box text-center">
									<a href="services-detail.html" class="theme-btn btn-style-three"><span class="txt">Learn More</span></a>
								</div>
							</div>
						</div>
						
						<!-- Tab -->
						<div class="tab active-tab" id="prod-css">
							<div class="content">
								<h4>CSS Website Development Services</h4>
								<div class="text">Conitec is the partner of choice for many of the world’s leading enterprises, SMEs and technology challengers. We help businesses <br> elevate their value through custom software development, product design, QA and consultancy services.</div>
								<div class="btn-box text-center">
									<a href="services-detail.html" class="theme-btn btn-style-three"><span class="txt">Learn More</span></a>
								</div>
							</div>
						</div>
						
						<!-- Tab -->
						<div class="tab" id="prod-php">
							<div class="content">
								<h4>Php Website Development Services</h4>
								<div class="text">Conitec is the partner of choice for many of the world’s leading enterprises, SMEs and technology challengers. We help businesses <br> elevate their value through custom software development, product design, QA and consultancy services.</div>
								<div class="btn-box text-center">
									<a href="services-detail.html" class="theme-btn btn-style-three"><span class="txt">Learn More</span></a>
								</div>
							</div>
						</div>
						
						<!-- Tab -->
						<div class="tab" id="prod-java">
							<div class="content">
								<h4>JavaScript Website Development Services</h4>
								<div class="text">Engitech is the partner of choice for many of the world’s leading enterprises, SMEs and technology challengers. We help businesses <br> elevate their value through custom software development, product design, QA and consultancy services.</div>
								<div class="btn-box text-center">
									<a href="services-detail.html" class="theme-btn btn-style-three"><span class="txt">Learn More</span></a>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!-- End Experiance Section -->
@endsection