@extends('layout.app')

@section('title', 'Contact Us')



@section('content')

<!--Page Title-->
    <section class="page-title">
		<div class="pattern-layer-one" style="background-image: url(images/background/pattern-16.png)"></div>
    	<div class="auto-container">
			<h2>Contact Us </h2>
			<h4> What are you waiting for? Call Us Today  </h4>
			<ul class="page-breadcrumb">
				<li><a href="index.html">home</a></li>
				<li>Contact us</li>
			</ul>
        </div>
    </section>
    <!--End Page Title-->
	
	<!-- Contact Info Section -->
	<section class="contact-info-section">
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="title-box">
				<div class="title">GET IN TOUCH</div>
				<div class="text">For genereal enquires you can touch with our front desk supporting team <br> at <a href="mailto:info@conitecservices.com">info@conitecservices.com</a> or call on <a href="tel:+966-11-2930449">+966-11-2930449</a></div>
			</div>
			
			<div class="row clearfix">
			
				<!-- Info Column -->
				<div class="info-column col-lg-4 col-md-6 col-sm-12">
					<div class="inner-column">
						<div class="content">
							<div class="icon-box"><span class="flaticon-pin"></span></div>
							<ul>
								<li><strong>Address</strong></li>
								<li>Dabab Street, Riyadh KSA </li>
							</ul>
						</div>
					</div>
				</div>
				
				<!-- Info Column -->
				<div class="info-column col-lg-4 col-md-6 col-sm-12">
					<div class="inner-column">
						<div class="content">
							<div class="icon-box"><span class="flaticon-phone-call"></span></div>
							<ul>
								<li><strong>Mobile</strong></li>
								<li> +966-540333650 </li>
							</ul>
						</div>
					</div>
				</div>
				
				<!-- Info Column -->
				<div class="info-column col-lg-4 col-md-6 col-sm-12">
					<div class="inner-column">
						<div class="content">
							<div class="icon-box"><span class="flaticon-email-1"></span></div>
							<ul>
								<li><strong>E-Mail</strong></li>
								<li>support@conitecservices.com</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</section>
	<!-- End Contact Info Section -->

<!-- Appointment Section -->
	<section class="appointment-section style-two">
		<div class="image-layer" style="background-image: url(images/background/4.jpg)"></div>
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="sec-title light centered">
				
				<h2>Join Us To Get IT  <br> Services </h2>
			</div>
			<div class="inner-container">
				<div class="row clearfix">
					
					<!-- Image Column -->
					<div class="image-column col-lg-6 col-md-12 col-sm-12">
						<div class="inner-column wow slideInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="image">
								<img src="images/resource/appointment.jpg" alt="" />
							</div>
						</div>
					</div>
					
					<!-- Form Column -->
					<div class="form-column col-lg-6 col-md-12 col-sm-12">
						<div class="inner-column">
							<h4>You Don't Hesitate To Contact <br> With Us, Now Say Hello......</h4>
							<!-- Appointment Form -->
							<div class="appointment-form">
								<form method="post" action="#">
									<div class="row clearfix">
										<div class="col-lg-6 col-md-6 col-sm-12 form-group">
											<input type="text" name="cname" placeholder="Company Name" required="">
											<span class="icon fa fa-building-o"></span>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-12 form-group">
											<input type="text" name="cname" placeholder="Contact Person" required="">
											<span class="icon fa fa-user"></span>
										</div>
										
										<div class="col-lg-6 col-md-6 col-sm-12 form-group">
											<input type="email" name="email" placeholder="Company Email" required="">
											<span class="icon fa fa-envelope"></span>
										</div>

										<div class="col-lg-6 col-md-6 col-sm-12 form-group">
											<input type="tel" name="mobile" placeholder="Mobile No" required="">
											<span class="icon fa fa-mobile"></span>
										</div>
										<h5> Select Services That You Need  </h5>
										<div class="col-lg-12 col-md-12 col-sm-12 form-group">
											<input type="checkbox"  value="" name="network"> Network 
											<input type="checkbox"  value="" name="camera">Camera Installation 
											<input type="checkbox"  value="" name="firewall">Firewall Installation 
											<input type="checkbox"  value="" name="voip">VOIP Installation   
											<input type="checkbox"  value="" name="cisco">Cisco Installation 
										</div>
										<div class="col-lg-6 col-md-6 col-sm-12 form-group">
											<select> 
									        <option> Select City </option>
									        <option value="Riyadh"> Riyadh</option>
									        <option value="Jeddah"> Jeddah</option>
									        <option value="Dammam"> Dammam</option>
									      </select>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-12 form-group">
											<input type="tel" name="address" placeholder="Office Address" required="">
											<span class="icon fa fa-map-marker"></span>
										</div>

										<div class="col-lg-12 col-md-12 col-sm-12 form-group">
											<textarea name="message" placeholder="Comments"></textarea>
										</div>

										<div class="col-lg-12 col-md-12 col-sm-12 form-group">
											<button class="theme-btn btn-style-three" type="submit" name="submit-form"><span class="txt">Submit Services Request </span></button>
										</div>
									</div>
								</form>
							</div>
							
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</section>


	
	<section class="contact-map-section">
		<div class="auto-container">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3625.271835906319!2d46.687588214474054!3d24.68318045842003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e2f035f1d8f44d1%3A0xf78c9174860469ee!2sConitec%20Group!5e0!3m2!1sen!2ssa!4v1597743476407!5m2!1sen!2ssa" width="100%" height="100px" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
			
	</section>
@endsection