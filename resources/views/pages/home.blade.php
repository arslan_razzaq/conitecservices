@extends('layout.app')

@section('title', 'Leading IT Services Provider In Saudi Arabia')



@section('content')
    <!-- Banner Section -->
    <section class="banner-section">
		<div class="main-slider-carousel owl-carousel owl-theme">
            
            <div class="slide" style="background-image: url(images/main-slider/image-4.jpg)">
				<div class="patern-layer-one" style="background-image: url(images/main-slider/pattern-1.png)"></div>
				<div class="patern-layer-two" style="background-image: url(images/main-slider/pattern-2.png)"></div>
				<div class="auto-container">
					
					<!-- Content Column -->
					<div class="content-column">
						<div class="inner-column">
							<div class="patern-layer-three" style="background-image: url(images/main-slider/pattern-3.png)"></div>
							<div class="title">IT Business Services Provider </div>
							<h1>YOUR ONE STOP SHOP FOR ALL YOUR IT NEEDS</h1>
							<div class="text">Since 2005, businesses and local residents of Saudi Arabia have trusted Conitecservices provider as their industry leader in IT management, telecommunications, security services and low voltage wiring. Our values are simple: serve our customers well, build long lasting relationships , and hire the most talented technicians and service staff who share these same values. We focus on technology, so you can focus on your business.</div>
							<div class="btns-box">
								<a href="about.html" class="theme-btn btn-style-one"><span class="txt">Learn More</span></a>
							</div>
						</div>
					</div>
					
				</div>
			</div>
			
			
			
			
			
		</div>
		
	</section>
	<!-- End Banner Section -->


	<!-- About Section -->
	<section class="about-section">
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="sec-title">
				
				<h2 class="text-center">We deal with the aspects of professional IT Services</h2>
			</div>
			<div class="row clearfix">
				
				 <div class="col-md-4 col-sm-6">
                    <div class="box8">
                        <img src="images/resource/phone.jpg" class="rounded">
                        <h3 class="title text-center">Voice </h3>
                        <div class="box-content">
                            <h2> IS YOUR OUTDATED PHONE SYSTEM COSTING YOU MONEY? </h2>
								<p> Enable greater levels of collaboration for users in the office or working from home! Take advantage of Unified Communications (UC) and collaboration tools – without a high price tag. Features like chat, video chat, quick dial, status, presence, and call rules all work together to allow your staff and clients to interact more efficiently. </p>
								<a href="about.html" class="theme-btn btn-style-one"><span class="txt">Learn More</span></a>
						</div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="box8">
                        <img src="images/resource/data.jpg" class="rounded">
                        <h3 class="title text-center">Data</h3>
                        <div class="box-content">
                           <h2> MANAGED IT SERVICES </h2>
							<p> Jaydien Network Solutions provides a full team of IT professionals who proactively maintain your infrastructure and support your employees and the technology they use. We become an integral member of your team, helping your employees and your company navigate the hurdles that come with technology. </p>
							<a href="about.html" class="theme-btn btn-style-one"><span class="txt">Learn More</span></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="box8">
                        <img src="images/resource/security.jpg" class="rounded">
                        <h3 class="title text-center">Security</h3>
                        <div class="box-content">
                            <h2> BUSINESS SECURITY SOLUTIONS </h2>
							<p> With the help of a good camera surveillance system, you don't have to physically be there to verify that things are running smoothly. A security camera system is also a deterrent for potential thieves and provides invaluable information if an incidence happens to occur on site. Virtually any home or business can benefit from the added protection and peace of mind that comes with having a security surveillance system </p>
							<a href="about.html" class="theme-btn btn-style-one"><span class="txt">Learn More</span></a>
                        </div>
                    </div>
                </div>
			</div>
			<br> <br> <br>
			<div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="box8">
                        <img src="images/resource/firewall.jpg" class="rounded">
                        <h3 class="title text-center">Network Security</h3>
                        <div class="box-content">
                            <h2> Fortinet Firewall </h2>
							<p> The FortiGate next-gen firewall is a high-performance network security appliance that adds intrusion prevention, application and user visibility, SSL inspection, and unknown threat detection to the traditional firewall. ... Deliver superior multi-function performance by running on purpose-built appliances with custom ASICs. </p>
							<a href="about.html" class="theme-btn btn-style-one"><span class="txt">Learn More</span></a>
                        </div>
                    </div>
                </div>
				
                <div class="col-md-4 col-sm-6">
                    <div class="box8">
                        <img src="images/resource/web.jpg" class="rounded">
                        <h3 class="title text-center">Web Development </h3>
                        <div class="box-content">
                            <h2> Web Application Development </h2>
							<p> Leading company in mobile friendly web development, design and maintenance, Call Us Now! 100’s of Satisfied Customers for Specialized CMS based Website Development in Saudi Arabia. Paid Search Marketing. Digital Marketing Agency. Social Media Marketing. </p>
							<a href="about.html" class="theme-btn btn-style-one"><span class="txt">Learn More</span></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="box8">
                        <img src="images/resource/cisco.jpg" class="rounded">
                        <h3 class="title text-center">Cisco Router </h3>
                        <div class="box-content">
                            <h2> Cisco Router Configuration </h2>
							<p> Next Generation endpoint protection and scanning files using a variety of antimalware tech. AMP goes a step further than most malware detection tools. Get A Free Consultation. Chat Support Available. Wide Spectrum Of Skills. </p>
							<a href="about.html" class="theme-btn btn-style-one"><span class="txt">Learn More</span></a>
                        </div>
                    </div>
                </div>
				
				
			</div>
		</div>
	</section>
	<!-- End About Section -->


<!-- Counter Section -->
	<section class="counter-section">
		<div class="auto-container">
			<div class="inner-container">
				<!-- Fact Counter -->
				<div class="fact-counter">
					<div class="row clearfix">

						<!-- Column -->
						<div class="column counter-column col-lg-3 col-md-6 col-sm-12">
							<div class="inner wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
								<div class="content">
									<div class="count-outer count-box">
										<span class="count-text" data-speed="3000" data-stop="330">0</span>+
									</div>
									<h4 class="counter-title">ACTIVE CLIENTS</h4>
								</div>
							</div>
						</div>

						<!-- Column -->
						<div class="column counter-column col-lg-3 col-md-6 col-sm-12">
							<div class="inner wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
								<div class="content">
									<div class="count-outer count-box alternate">
										<span class="count-text" data-speed="5000" data-stop="850">0</span>+
									</div>
									<h4 class="counter-title">PROJECTS DONE</h4>
								</div>
							</div>
						</div>

						<!-- Column -->
						<div class="column counter-column col-lg-3 col-md-6 col-sm-12">
							<div class="inner wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
								<div class="content">
									<div class="count-outer count-box">
										<span class="count-text" data-speed="2000" data-stop="25">0</span>+
									</div>
									<h4 class="counter-title">TEAM ADVISORS</h4>
								</div>
							</div>
						</div>

						<!-- Column -->
						<div class="column counter-column col-lg-3 col-md-6 col-sm-12">
							<div class="inner wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
								<div class="content">
									<div class="count-outer count-box">
										<span class="count-text" data-speed="3500" data-stop="10">0</span>+
									</div>
									<h4 class="counter-title">GLORIOUS YEARS</h4>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Counter Section -->


	
	<br> <br> 
	
	
	<!-- Call To Action Section -->
	<section class="call-to-action-section" style="background-image: url(images/background/pattern-3.png)">
		<div class="auto-container">
			<div class="row clearfix">
				<!-- Heading Column -->
				<div class="heading-column col-lg-8 col-md-12 col-sm-12">
					<div class="inner-column">
						<h2>Preparing For Your Business <br> Success With IT Solution</h2>
					</div>
				</div>
				<!-- Button Column -->
				<div class="heading-column col-lg-4 col-md-12 col-sm-12">
					<div class="inner-column">
						<h2> CALL US TODAY! +966-11-293 0449 </h2>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Call To Action Section -->
	
	<br> <br> 
	
	<!--Sponsors Section-->
	<section class="sponsors-section">
		<div class="auto-container">
			
			<div class="carousel-outer">
                <!--Sponsors Slider-->
                <ul class="sponsors-carousel owl-carousel owl-theme">
                    <li><div class="image-box"><a href="#"><img src="images/clients/1.png" alt=""></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="images/clients/2.png" alt=""></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="images/clients/3.png" alt=""></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="images/clients/4.png" alt=""></a></div></li>
					<li><div class="image-box"><a href="#"><img src="images/clients/1.png" alt=""></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="images/clients/2.png" alt=""></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="images/clients/3.png" alt=""></a></div></li>
                    <li><div class="image-box"><a href="#"><img src="images/clients/4.png" alt=""></a></div></li>
                </ul>
            </div>
			
		</div>
	</section>
	<!--End Sponsors Section-->
	
	<br> <br> 
	
	<!-- Technology Section -->
	<section class="technology-section" style="background-image: url(images/background/1.jpg)">
		<div class="pattern-layer-one" style="background-image: url(images/background/pattern-5.png)"></div>
		<div class="pattern-layer-two" style="background-image: url(images/background/pattern-6.png)"></div>
		<div class="auto-container">
			<div class="row clearfix">
				<!-- Title Column -->
				<div class="title-column col-lg-5 col-md-12 col-sm-12">
					<div class="inner-column">
						<!-- Sec Title -->
						<div class="sec-title light">
							<div class="title">TECHNOLOGY INDEX</div>
							<h2>We Deliver Solutions with the Goal of Trusting Workships</h2>
						</div>
					</div>
				</div>
				<!-- Blocks Column -->
				<div class="blocks-column col-lg-7 col-md-12 col-sm-12">
					<div class="inner-column">
						<div class="row clearfix">
							
							<!-- Technology Block -->
							<div class="technology-block col-lg-4 col-md-6 col-sm-12">
								<div class="inner-box">
									<a href="services-detail.html" class="overlay-link"></a>
									<div class="icon-box">
										<span class="flaticon-coding-2"></span>
									</div>
									<h6>WEB</h6>
								</div>
							</div>
							
							<!-- Technology Block -->
							<div class="technology-block col-lg-4 col-md-6 col-sm-12">
								<div class="inner-box">
									<a href="services-detail.html" class="overlay-link"></a>
									<div class="icon-box">
										<span class="flaticon-android"></span>
									</div>
									<h6>ANDROID</h6>
								</div>
							</div>
							
							<!-- Technology Block -->
							<div class="technology-block col-lg-4 col-md-6 col-sm-12">
								<div class="inner-box">
									<a href="services-detail.html" class="overlay-link"></a>
									<div class="icon-box">
										<span class="flaticon-apple"></span>
									</div>
									<h6>IOS</h6>
								</div>
							</div>
							
							<!-- Technology Block -->
							<div class="technology-block col-lg-4 col-md-6 col-sm-12">
								<div class="inner-box">
									<a href="services-detail.html" class="overlay-link"></a>
									<div class="icon-box">
										<span class="flaticon-iot"></span>
									</div>
									<h6>IOT</h6>
								</div>
							</div>
							
							<!-- Technology Block -->
							<div class="technology-block col-lg-4 col-md-6 col-sm-12">
								<div class="inner-box">
									<a href="services-detail.html" class="overlay-link"></a>
									<div class="icon-box">
										<span class="flaticon-smartband"></span>
									</div>
									<h6>WEARALABLES</h6>
								</div>
							</div>
							
							<!-- Technology Block -->
							<div class="technology-block col-lg-4 col-md-6 col-sm-12">
								<div class="inner-box">
									<a href="services-detail.html" class="overlay-link"></a>
									<div class="icon-box">
										<span class="flaticon-tv"></span>
									</div>
									<h6>TV</h6>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Technology Section -->

	<!-- Team Section -->
	<section class="team-section" style="background-image: url(images/background/2.jpg)">
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="sec-title">
				<div class="clearfix">
					<div class="pull-left">
						<div class="title">OUR DEDICATED TEAM</div>
						<h2>We have Large No <br> of Expert Team Member</h2>
					</div>
					<div class="pull-right">
						<div class="text">Our goal is to help our companies maintain or achieve best- in-class <br> positions in their respective industries and our team works.</div>
					</div>
				</div>
			</div>
			<div class="clearfix">
				
				<!-- Team Block -->
				<div class="team-block col-lg-3 col-md-6 col-sm-12">
					<div class="inner-box">
						<div class="image">
							<a href="team.html"><img src="images/resource/team-1.jpg" alt="" /></a>
						</div>
						<div class="lower-box">
							<!-- Social Box -->
							<ul class="social-box">
								<li><a href="#" class="fa fa-facebook-f"></a></li>
								<li><a href="#" class="fa fa-twitter"></a></li>
								<li><a href="#" class="fa fa-dribbble"></a></li>
							</ul>
							<div class="content">
								<h5><a href="team.html">Jennifer Garcia</a></h5>
								<div class="designation">Consultant Officer</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Team Block -->
				<div class="team-block col-lg-3 col-md-6 col-sm-12">
					<div class="inner-box">
						<div class="image">
							<a href="team.html"><img src="images/resource/team-2.jpg" alt="" /></a>
						</div>
						<div class="lower-box">
							<!-- Social Box -->
							<ul class="social-box">
								<li><a href="#" class="fa fa-facebook-f"></a></li>
								<li><a href="#" class="fa fa-twitter"></a></li>
								<li><a href="#" class="fa fa-dribbble"></a></li>
							</ul>
							<div class="content">
								<h5><a href="team.html">Robert Liam</a></h5>
								<div class="designation">Web Designer</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Team Block -->
				<div class="team-block col-lg-3 col-md-6 col-sm-12">
					<div class="inner-box">
						<div class="image">
							<a href="team.html"><img src="images/resource/team-3.jpg" alt="" /></a>
						</div>
						<div class="lower-box">
							<!-- Social Box -->
							<ul class="social-box">
								<li><a href="#" class="fa fa-facebook-f"></a></li>
								<li><a href="#" class="fa fa-twitter"></a></li>
								<li><a href="#" class="fa fa-dribbble"></a></li>
							</ul>
							<div class="content">
								<h5><a href="team.html">Jassica Ethan</a></h5>
								<div class="designation">Project Manager</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Team Block -->
				<div class="team-block col-lg-3 col-md-6 col-sm-12">
					<div class="inner-box">
						<div class="image">
							<a href="team.html"><img src="images/resource/team-4.jpg" alt="" /></a>
						</div>
						<div class="lower-box">
							<!-- Social Box -->
							<ul class="social-box">
								<li><a href="#" class="fa fa-facebook-f"></a></li>
								<li><a href="#" class="fa fa-twitter"></a></li>
								<li><a href="#" class="fa fa-dribbble"></a></li>
							</ul>
							<div class="content">
								<h5><a href="team.html">Adaim Mrala</a></h5>
								<div class="designation">IT Service Officer</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!-- End Team Section -->
@endsection